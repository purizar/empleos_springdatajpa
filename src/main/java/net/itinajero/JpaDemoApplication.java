package net.itinajero;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import net.itinajero.model.Categoria;
import net.itinajero.repository.CategoriasRepository;

@SpringBootApplication
public class JpaDemoApplication implements CommandLineRunner{

	@Autowired
	private CategoriasRepository repo;
	
	public static void main(String[] args) {
		SpringApplication.run(JpaDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(repo);
		//guardar();
		//buscarPorId();
		//modificar();
		//eliminar();
		//conteo();
		//eliminarTodos();
		//conteo();
		//encontrarPorIds();
		//buscarTodos();
		//existeId();
		//guardarTodas();
		//buscarTodasJpa();
		//borrarTodoEnbloque() ;
		//buscarTodosOrdenados();
		//buscarTodosPaginacion();
		buscarTodosPaginacionOrdenado();
		
	}
	
	private void guardar() {
		System.out.println("Insertando un registro");
		Categoria cat = new Categoria();
		cat.setNombre("Finanzas");
		cat.setDescripcion("Trabajo relñacionado con finanazas");
		repo.save(cat);
		//System.out.println(cat);
	}
	
	/**
	 * Metodo findById - Interfaz de CrudRepository
	 */
	private void buscarPorId() {
		Optional<Categoria> optional = repo.findById(10);
		if (optional.isPresent()==true)
			System.out.println(optional.get());
		else
			System.out.println("categoria no encontrada");
		
	}
	
	/**
	 * Metodo save(update) - Interfaz de CrudRepository
	 */
	private void modificar() {
				
		Optional<Categoria> optional = repo.findById(2);
		if (optional.isPresent()==true) {
			Categoria catTmp = optional.get();
			catTmp.setNombre("ING. SISTEMAS");
			catTmp.setDescripcion("Puesto para ingenieria de sistemas");
			repo.save(catTmp);
			System.out.println(optional.get());
		}
		else
			System.out.println("categoria no encontrada");
		
	}
	
	/**
	 * Metodo deleteById - Interfaz de CrudRepository
	 */
	public void eliminar() {
		int idCategoria =1;
		repo.deleteById(idCategoria);
		System.out.println("Eliminando un registro");
	}
	
	/**
	 * Metodo count - Interfaz de CrudRepository
	 */
	public void conteo() {
		long count = repo.count();
		System.out.println("Total Categorias: "+ count);
	}

	/**
	 * Metodo deleteAll - Interfaz de CrudRepository
	 */
	public void eliminarTodos() {
		repo.deleteAll();
		
	}
	
	/**
	 * Metodo findAllById - Interfaz de CrudRepository
	 */
	public void encontrarPorIds() {
		List<Integer> ids= new LinkedList<Integer>(); 
		ids.add(1);
		ids.add(4);
		ids.add(10);
		Iterable<Categoria> categorias = repo.findAllById(ids);
		for (Categoria cat:categorias) {
			System.out.println(cat);
		}
		
		
	}
	
	/**
	 * Metodo findAll - Interfaz de CrudRepository
	 */
	public void buscarTodos() {
		Iterable<Categoria> categorias = repo.findAll();
		for (Categoria cat:categorias) {
			System.out.println(cat);
		}
		
		
	}
	
	/**
	 * Metodo existsById - Interfaz de CrudRepository
	 */
	public void existeId() {
     boolean existe = repo.existsById(5);
     if (existe) {
			System.out.println("la categoria existe");
		}
		
	}
	
	/**
	 * Metodo saveAll - Interfaz de CrudRepository
	 */
	private void guardarTodas() {
		List<Categoria> categorias = getListaCategorias();
		repo.saveAll(categorias);
		//System.out.println(cat);
	}
	
	/**
	 * Metodo que regresa una lista de 3 Categorias
	 * @return
	 */
	private List<Categoria> getListaCategorias(){
		List<Categoria> lista = new LinkedList<Categoria>();
		// Categoria 1
		Categoria cat1 = new Categoria();
		cat1.setNombre("Programador de Blockchain");
		cat1.setDescripcion("Trabajos relacionados con Bitcoin y Criptomonedas");
		
		// Categoria 2
		Categoria cat2 = new Categoria();
		cat2.setNombre("Soldador/Pintura");
		cat2.setDescripcion("Trabajos relacionados con soldadura, pintura y enderezado");
						
		// Categoria 3
		Categoria cat3 = new Categoria();
		cat3.setNombre("Ingeniero Industrial");
		cat3.setDescripcion("Trabajos relacionados con Ingenieria industrial.");
		
		lista.add(cat1);
		lista.add(cat2);
		lista.add(cat3);
		return lista;
	}
	
	/**
	 * Metodo deleteAllInBatch - Interfaz de JpaRepository
	 */
	private void borrarTodoEnbloque() {
		repo.deleteAllInBatch();
	}
	
	/**
	 * Metodo findAll (ordenados por un campo)- Interfaz de JpaRepository
	 */
	private void buscarTodosOrdenados() {
		Iterable<Categoria> categorias = repo.findAll(Sort.by("nombre").descending());
		for (Categoria c:categorias) {
			System.out.println(c.getId() + " " + c.getNombre());
		}
	}
	
	/**
	 * Metodo findAll (con paginacion)- Interfaz de PagingAndSortingRepository
	 */
	private void buscarTodosPaginacion() {
		Page<Categoria> page = repo.findAll(PageRequest.of(0,5));
		System.out.println("Total registros "+ page.getTotalElements() );
		System.out.println("Total paginas "+ page.getTotalPages());
		for (Categoria c:page.getContent()) {
			System.out.println(c.getId() + " " + c.getNombre());
		}
	}
	
	/**
	 * Metodo findAll (con paginacion y ordenados)- Interfaz de PagingAndSortingRepository
	 */
	private void buscarTodosPaginacionOrdenado() {
		Page<Categoria> page = repo.findAll(PageRequest.of(1,5, Sort.by("nombre")));
		System.out.println("Total registros "+ page.getTotalElements() );
		System.out.println("Total paginas "+ page.getTotalPages());
		for (Categoria c:page.getContent()) {
			System.out.println(c.getId() + " " + c.getNombre());
		}
	}
	
}
