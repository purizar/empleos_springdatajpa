package net.itinajero.repository;

import org.springframework.data.repository.CrudRepository;

import net.itinajero.model.Categoria;

public interface CategoriasRepository_CrudRepository extends CrudRepository<Categoria, Integer> {

}
